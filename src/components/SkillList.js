export default function SkillList(props) {
    const style = props.bgColor;
  return (
    <div className="skill" id={props.id} style={{backgroundColor: style}}>
      <span>{props.text}</span>
    </div>
  );
}
