export default function Intro(props) {
  return (
    <div className="intro">
      <h1>Braily Guzman</h1>
      <p>
        Front-end web developer and High School student. When I'm not writing
        code, I am most likely playing video games, especially Microsoft Flight
        Simulator.
      </p>
    </div>
  );
}
