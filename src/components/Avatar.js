export default function Avatar(props) {
  return (
    <div className="avatar">
      <img src={props.avatar} alt="Avatar" />
    </div>
  );
}
