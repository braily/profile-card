import Avatar from "./components/Avatar";
import Intro from "./components/Intro";
import SkillList from "./components/SkillList";
import "./css/index.css";
import avatarImage from "./images/me.png";

function App() {
  return (
    <div className="card-container">
      <div className="card">
        <Avatar avatar={avatarImage} />
        <div className="data">
          <Intro />
          <div class="skillList">
            <SkillList bgColor="#FFFF00" text="JavaScript 💛" />
            <SkillList bgColor="#4584b6" text="Python 🐍" />
            <SkillList bgColor="#fff" text="Web Design 🖥️" />
            <SkillList bgColor="#FF0000" text="C# 🧑‍💻" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
